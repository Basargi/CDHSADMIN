﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CDHS_ADMIN.Models;
namespace CDHS_ADMIN.Controllers

{
    public class InfoController : Controller
    {
        private cdhsCommonMethods _cdhsCommonMethods = new cdhsCommonMethods();


        #region GP_cdhs
        public ActionResult viewgp()
        {
            IEnumerable<GP> _GP = _cdhsCommonMethods.GetGpcdhsData();
            ViewGpViewModel _ViewGpViewModel = new ViewGpViewModel();
            // Action list for temprory use delete it later
            _ViewGpViewModel.ActionList = new List<ActionListModel>{
                new ActionListModel{
                    ActionId = "1", ActionStatus = "Action"
                },
                new ActionListModel{
                    ActionId = "2", ActionStatus = "View"
                },
                new ActionListModel{
                    ActionId = "3", ActionStatus = "Edit"
                },
                new ActionListModel{
                    ActionId = "4", ActionStatus = "Delete"
                }
            };

            _ViewGpViewModel.gpcdhs = _GP;

            return View(_ViewGpViewModel);
        }
        [HttpPost]
        public ActionResult viewgp(FormCollection form_collection)
        {
            int result = 0;
            if (form_collection["basicOps"] != null && !string.IsNullOrEmpty(form_collection["basicOps"].Replace(",", "")))
            {
                string[] opType = form_collection["basicOps"].ToString().Split('~');
                if (opType[1].IndexOf("Edit") > -1)
                {
                    return RedirectToAction("addgp", "Info", new { id = opType[0].ToString().Trim().Replace(",", ""), edid = opType[0].ToString().Trim().Replace(",", "") });
                }
                else if (opType[1].IndexOf("Delete") > -1)
                {
                    //result = _ftwCommonMethods.AboutFtwDelete(opType[0].ToString().Replace(",", ""));
                    //if (result == 1)
                    //{
                    //    ViewData["message"] = "Record Deleted Successfully";
                    //}
                    //else
                    //{
                    //    ViewData["message"] = "Record Delete Failed";
                    //}
                }
            }
            return RedirectToAction("viewgp", "Info");
        }
        public ActionResult addgp()
        {
            AddgpViewModel _AddgpViewModel = new AddgpViewModel();
            if (Request.QueryString["edid"] != null)
            {
                GP _GP = new GP();
                _GP = _cdhsCommonMethods.GetDataGpById(Request.QueryString["edid"].ToString());

                _AddgpViewModel.ClinicName = _GP.Clinic_Name;
                _AddgpViewModel.ContactName = _GP.Contact_Name;
                _AddgpViewModel.MobileNo = _GP.Mobile_No;
                _AddgpViewModel.EmailID = _GP.Email_ID;
                _AddgpViewModel.EnterPassword = _GP.Enter_Password;
                _AddgpViewModel.EnterConfirmPassword = _GP.Enter_Confirm_Password;
                _AddgpViewModel.Adddress = _GP.Adddress;
                _AddgpViewModel.Subrub = _GP.Subrub;
                _AddgpViewModel.State = _GP.State;
                _AddgpViewModel.Postcode = _GP.Postcode;

                _AddgpViewModel.id = _GP.id;
                _AddgpViewModel.Priority = _GP.Priority;
                //if (_AboutFtw.title_img != null)
                //{
                //    string imagefoldername = _AboutFtwViewModel.image.Substring(0, _AboutFtwViewModel.image.IndexOf('.'));
                //    DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + imagefoldername));
                //    if (dir.Exists)
                //    {
                //        FileInfo[] allfiles = dir.GetFiles();
                //        _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + imagefoldername + "/" + allfiles[0].Name);
                //        // _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + FolderName + "/" + allfiles[0].Name);
                //    }
                //}
                ////  string FolderName = _AboutFtw.title_img;
                //// FolderName = FolderName.Substring(0, FolderName.LastIndexOf('.'));                
                //// DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + FolderName));

                //TempData["oldimage"] = _AboutFtw.title_img;
                //TempData.Keep("oldimage");
                return View(_AddgpViewModel);
            }
            return View(_AddgpViewModel);
        }
        [HttpPost]
        public ActionResult addgp(FormCollection _FormCollect)
        {
            int result = 0;
            if (_FormCollect["id"] != null && _FormCollect["id"].ToString() != "")
            {
                GP dbaddaboutcdhs = new GP();
                dbaddaboutcdhs.Clinic_Name = _FormCollect["ClinicName"].ToString();
                dbaddaboutcdhs.Contact_Name = _FormCollect["ContactName"].ToString();
                dbaddaboutcdhs.Mobile_No = _FormCollect["MobileNo"].ToString();
                dbaddaboutcdhs.Email_ID = _FormCollect["EmailID"].ToString();
                dbaddaboutcdhs.Enter_Password = _FormCollect["EnterPassword"].ToString();
                dbaddaboutcdhs.Enter_Confirm_Password = _FormCollect["EnterConfirmPassword"].ToString();
                dbaddaboutcdhs.Adddress = _FormCollect["Adddress"].ToString();
                dbaddaboutcdhs.Subrub = _FormCollect["Subrub"].ToString();
                dbaddaboutcdhs.State = _FormCollect["State"].ToString();
                dbaddaboutcdhs.Postcode = HttpUtility.HtmlDecode(_FormCollect["Postcode"].ToString());
                dbaddaboutcdhs.id = _FormCollect["id"].ToString();
                TempData.Clear();
                result = _cdhsCommonMethods.addgpEdit(dbaddaboutcdhs);
            }
            else
            {
                GP dbaddaboutcdhs = new GP();
                dbaddaboutcdhs.Clinic_Name = _FormCollect["ClinicName"].ToString();
                dbaddaboutcdhs.Contact_Name = _FormCollect["ContactName"].ToString();
                dbaddaboutcdhs.Mobile_No = _FormCollect["MobileNo"].ToString();
                dbaddaboutcdhs.Email_ID = _FormCollect["EmailID"].ToString();
                dbaddaboutcdhs.Enter_Password = _FormCollect["EnterPassword"].ToString();
                dbaddaboutcdhs.Enter_Confirm_Password = _FormCollect["EnterConfirmPassword"].ToString();
                dbaddaboutcdhs.Adddress = _FormCollect["Adddress"].ToString();
                dbaddaboutcdhs.Subrub = _FormCollect["Subrub"].ToString();
                dbaddaboutcdhs.State = _FormCollect["State"].ToString();
                dbaddaboutcdhs.Postcode = HttpUtility.HtmlDecode(_FormCollect["Postcode"].ToString());
                dbaddaboutcdhs.id = Guid.NewGuid().ToString().Trim();
                TempData.Clear();
                result = _cdhsCommonMethods.addgpAdd(dbaddaboutcdhs);
            }
            return View(new AddgpViewModel());
        }
        #endregion

        #region Sales_cdhs
        //[AuthorizeUser]

        public ActionResult viewsalesrep()
        {
            IEnumerable<Sales_Rep> _Sales_Rep = _cdhsCommonMethods.GetSalesrepcdhsData();
            ViewSalesRepViewModel _ViewSalesRepViewModel = new ViewSalesRepViewModel();
            // Action list for temprory use delete it later
            _ViewSalesRepViewModel.ActionList = new List<ActionListModel>{
                new ActionListModel{
                    ActionId = "1", ActionStatus = "Action"
                },
                new ActionListModel{
                    ActionId = "2", ActionStatus = "View"
                },
                new ActionListModel{
                    ActionId = "3", ActionStatus = "Edit"
                },
                new ActionListModel{
                    ActionId = "4", ActionStatus = "Delete"
                }
            };

            _ViewSalesRepViewModel.salescdhs = _Sales_Rep;

            return View(_ViewSalesRepViewModel);
        }
        [HttpPost]
        public ActionResult viewsalesrep(FormCollection form_collection)
        {
            int result = 0;
            if (form_collection["basicOps"] != null && !string.IsNullOrEmpty(form_collection["basicOps"].Replace(",", "")))
            {
                string[] opType = form_collection["basicOps"].ToString().Split('~');
                if (opType[1].IndexOf("Edit") > -1)
                {
                    return RedirectToAction("addsalesrep", "Info", new { id = opType[0].ToString().Trim().Replace(",", ""), edid = opType[0].ToString().Trim().Replace(",", "") });
                }
                else if (opType[1].IndexOf("Delete") > -1)
                {
                    //result = _ftwCommonMethods.AboutFtwDelete(opType[0].ToString().Replace(",", ""));
                    //if (result == 1)
                    //{
                    //    ViewData["message"] = "Record Deleted Successfully";
                    //}
                    //else
                    //{
                    //    ViewData["message"] = "Record Delete Failed";
                    //}
                }
            }
            return RedirectToAction("viewaddsalesrep", "Info");
        }

        public ActionResult addsalesrep()
        {
            AddSalesRepViewModel _AddSalesRepViewModel = new AddSalesRepViewModel();
            if (Request.QueryString["edid"] != null)
            {
                Sales_Rep _Sales_Rep = new Sales_Rep();
                _Sales_Rep = _cdhsCommonMethods.GetDataSalesrepById(Request.QueryString["edid"].ToString());

                _AddSalesRepViewModel.FirstName = _Sales_Rep.First_Name;
                _AddSalesRepViewModel.LastName = _Sales_Rep.Last_Name;
                _AddSalesRepViewModel.EmailID = _Sales_Rep.Email_ID;
                _AddSalesRepViewModel.EnterPassword = _Sales_Rep.Enter_Password;
                _AddSalesRepViewModel.ConfirmPassword = _Sales_Rep.Confirm_Password;
                _AddSalesRepViewModel.MobileNo = _Sales_Rep.Mobile_No;
                _AddSalesRepViewModel.Subrub = _Sales_Rep.Subrub;
                _AddSalesRepViewModel.State = _Sales_Rep.State;
                _AddSalesRepViewModel.Postcode = _Sales_Rep.Postcode;
                _AddSalesRepViewModel.id = _Sales_Rep.id;
                _AddSalesRepViewModel.Priority = _Sales_Rep.Priority;
                //if (_AboutFtw.title_img != null)
                //{
                //    string imagefoldername = _AboutFtwViewModel.image.Substring(0, _AboutFtwViewModel.image.IndexOf('.'));
                //    DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + imagefoldername));
                //    if (dir.Exists)
                //    {
                //        FileInfo[] allfiles = dir.GetFiles();
                //        _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + imagefoldername + "/" + allfiles[0].Name);
                //        // _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + FolderName + "/" + allfiles[0].Name);
                //    }
                //}
                ////  string FolderName = _AboutFtw.title_img;
                //// FolderName = FolderName.Substring(0, FolderName.LastIndexOf('.'));                
                //// DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + FolderName));

                //TempData["oldimage"] = _AboutFtw.title_img;
                //TempData.Keep("oldimage");
                return View(_AddSalesRepViewModel);
            }
            return View(_AddSalesRepViewModel);
        }
        [HttpPost]
        public ActionResult addsalesrep(FormCollection _FormCollect)
        {
            int result = 0;
            if (_FormCollect["id"] != null && _FormCollect["id"].ToString() != "")
            {
                Sales_Rep _Sales_Rep = new Sales_Rep();
                _Sales_Rep.First_Name = _FormCollect["FirstName"].ToString();
                _Sales_Rep.Last_Name = _FormCollect["LastName"].ToString();
                _Sales_Rep.Email_ID = _FormCollect["EmailID"].ToString();
                _Sales_Rep.Enter_Password = _FormCollect["EnterPassword"].ToString();
                _Sales_Rep.Confirm_Password = _FormCollect["ConfirmPassword"].ToString();
                _Sales_Rep.Mobile_No = _FormCollect["MobileNo"].ToString();
                _Sales_Rep.Subrub = _FormCollect["Subrub"].ToString();
                _Sales_Rep.State = _FormCollect["State"].ToString();
                _Sales_Rep.Postcode = _FormCollect["Postcode"].ToString();
                //_Sales_Rep.Priority = HttpUtility.HtmlDecode(_FormCollect["Priority"].ToString());
                _Sales_Rep.id = _FormCollect["id"].ToString();
                TempData.Clear();
                result = _cdhsCommonMethods.addsalesrepcdhsEdit(_Sales_Rep);
            }
            else
            {
                Sales_Rep _Sales_Rep = new Sales_Rep();
                _Sales_Rep.First_Name = _FormCollect["FirstName"].ToString();
                _Sales_Rep.Last_Name = _FormCollect["LastName"].ToString();
                _Sales_Rep.Email_ID = _FormCollect["EmailID"].ToString();
                _Sales_Rep.Enter_Password = _FormCollect["EnterPassword"].ToString();
                _Sales_Rep.Confirm_Password = _FormCollect["ConfirmPassword"].ToString();
                _Sales_Rep.Mobile_No = _FormCollect["MobileNo"].ToString();
                _Sales_Rep.Subrub = _FormCollect["Subrub"].ToString();
                _Sales_Rep.State = _FormCollect["State"].ToString();
                _Sales_Rep.Postcode = _FormCollect["Postcode"].ToString();
                //_Sales_Rep.Priority = HttpUtility.HtmlDecode(_FormCollect["Priority"].ToString());
                _Sales_Rep.id = Guid.NewGuid().ToString().Trim();
                TempData.Clear();
                result = _cdhsCommonMethods.addsalesrepcdhsAdd(_Sales_Rep);
            }
            return View(new AddSalesRepViewModel());
        }
        #endregion

        public ActionResult addvideochannel()
        {
            AddVideoChannelViewModel _AddVideoChannelViewModel = new AddVideoChannelViewModel();
            _AddVideoChannelViewModel.ApprovalList = new List<ApprovalListModel>{
                new ApprovalListModel{
                    ApprovalId = "1", ApprovalStatus = "Awaiting For Approval"
                },
                new ApprovalListModel{
                    ApprovalId = "2", ApprovalStatus = "Approved"
                },
                new ApprovalListModel{
                    ApprovalId = "3", ApprovalStatus = "Reject"
                },
            };
            return View(_AddVideoChannelViewModel);
        }
        public ActionResult allocategp()
        {
            AlLocateGpViewModel _AlLocateGpViewModel = new AlLocateGpViewModel();
            _AlLocateGpViewModel.SearchList = new List<SearchListModel>{
                new SearchListModel{
                    SearchId = "1", SearchStatus = "Select"
                },
                new SearchListModel{
                    SearchId = "2", SearchStatus = "Nick Franco"
                },
                new SearchListModel{
                    SearchId = "3", SearchStatus = "Mike Hussy"
                },
            };
            return View(_AlLocateGpViewModel);
        }
        public ActionResult registeredgp()
        {
            RegisteredGpViewModel _RegisteredGpViewModel = new RegisteredGpViewModel();
            _RegisteredGpViewModel.StateList = new List<StateListModel>();

            _RegisteredGpViewModel.CityList = new List<CityListModel>();

            return View(_RegisteredGpViewModel);
        }
        public ActionResult viewallocatedgp()
        {
            return View();
        }
        public ActionResult viewdiary()
        {
            return View();
        }     
        public ActionResult salesrepreport()
        {
            return View();
        }
        public ActionResult viewvideochannel()
        {
            ViewVideoChannelViewModel _ViewVideoChannelViewModel = new ViewVideoChannelViewModel();
            _ViewVideoChannelViewModel.ApprovalList = new List<ApprovalListModel>{
                new ApprovalListModel{
                    ApprovalId = "1", ApprovalStatus = "Awaiting For Approval"
                },
                new ApprovalListModel{
                    ApprovalId = "2", ApprovalStatus = "Approved"
                },
                new ApprovalListModel{
                    ApprovalId = "3", ApprovalStatus = "Reject"
                },
            };

            _ViewVideoChannelViewModel.ActionList = new List<ActionListModel>{
                new ActionListModel{
                    ActionId = "1", ActionStatus = "Action"
                },
                new ActionListModel{
                    ActionId = "2", ActionStatus = "View"
                },
                new ActionListModel{
                    ActionId = "3", ActionStatus = "Edit"
                },
                new ActionListModel{
                    ActionId = "4", ActionStatus = "Delete"
                }
            };
            return View(_ViewVideoChannelViewModel);
        }
        public ActionResult allocatedstatus()
        {
            AllocatedStatusViewModel _AllocatedStatusViewModel = new AllocatedStatusViewModel();
            _AllocatedStatusViewModel.SearchList = new List<SearchListModel>{
                new SearchListModel{
                    SearchId = "1", SearchStatus = "Search By Status"
                },
                new SearchListModel{
                    SearchId = "2", SearchStatus = "Pending"
                },
                new SearchListModel{
                    SearchId = "3", SearchStatus = "Registered"
                },
                new SearchListModel{
                    SearchId = "4", SearchStatus = "Next Visit"
                },
                new SearchListModel{
                    SearchId = "5", SearchStatus = "Not Interested"
                }
            };
            return View(_AllocatedStatusViewModel);
        }
	}
}