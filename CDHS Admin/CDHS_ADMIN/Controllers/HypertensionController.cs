﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CDHS_ADMIN.Models;

namespace CDHS_ADMIN.Controllers
{
    public class HypertensionController : Controller
    {
        private cdhsCommonMethods _cdhsCommonMethods = new cdhsCommonMethods();
        //
        // GET: /Hypertension/
        public ActionResult addhypertensioncontent()
        {
            AddHypertensionContentViewModel _AddHypertensionContentViewModel = new AddHypertensionContentViewModel();
            _AddHypertensionContentViewModel.PriorityList = new List<PriorityListModel>{
                new PriorityListModel{
                    id = "1", Priority = "Top"
                },
                new PriorityListModel{
                    id = "2", Priority = "Low"
                }
            };

            _AddHypertensionContentViewModel.ApprovalList = new List<ApprovalListModel>{
                new ApprovalListModel{
                    ApprovalId = "1", ApprovalStatus = "Awaiting For Approval"
                },
                new ApprovalListModel{
                    ApprovalId = "2", ApprovalStatus = "Approved"
                },
                new ApprovalListModel{
                    ApprovalId = "3", ApprovalStatus = "Reject"
                }
            };

            _AddHypertensionContentViewModel.AssetList = new List<AssetListModel>{
                new AssetListModel{
                    AssetId = "1", AssetStatus = "Image"
                },
                new AssetListModel{
                    AssetId = "2", AssetStatus = "Video"
                }
            };
            return View(_AddHypertensionContentViewModel);
        }
        public ActionResult addhypertensionsymptoms()
        {
            AddHypertensionSymptomsViewModel _AddHypertensionSymptomsViewModel = new AddHypertensionSymptomsViewModel();
            _AddHypertensionSymptomsViewModel.ApprovalList = new List<ApprovalListModel>{
                new ApprovalListModel{
                    ApprovalId = "1", ApprovalStatus = "Awaiting For Approval"
                },
                new ApprovalListModel{
                    ApprovalId = "2", ApprovalStatus = "Approved"
                },
                new ApprovalListModel{
                    ApprovalId = "3", ApprovalStatus = "Reject"
                }
            };

            _AddHypertensionSymptomsViewModel.AssetList = new List<AssetListModel>{
                new AssetListModel{
                    AssetId = "1", AssetStatus = "Image"
                },
                new AssetListModel{
                    AssetId = "2", AssetStatus = "Video"
                }
            };
            return View(_AddHypertensionSymptomsViewModel);
        }
        public ActionResult viewhypertensioncontent()
        {
            ViewHypertensionSymptomsViewModel _ViewHypertensionSymptomsViewModel = new ViewHypertensionSymptomsViewModel();
            _ViewHypertensionSymptomsViewModel.ApprovalList = new List<ApprovalListModel>{
                new ApprovalListModel{
                    ApprovalId = "1", ApprovalStatus = "Awaiting For Approval"
                },
                new ApprovalListModel{
                    ApprovalId = "2", ApprovalStatus = "Approved"
                },
                new ApprovalListModel{
                    ApprovalId = "3", ApprovalStatus = "Reject"
                }
            };

            _ViewHypertensionSymptomsViewModel.AssetList = new List<AssetListModel>{
                new AssetListModel{
                    AssetId = "1", AssetStatus = "Action"
                },
                new AssetListModel{
                    AssetId = "2", AssetStatus = "View"
                },
                                new AssetListModel{
                    AssetId = "3", AssetStatus = "Edit"
                },
                                new AssetListModel{
                    AssetId = "4", AssetStatus = "Delete"
                }
            };
            return View(_ViewHypertensionSymptomsViewModel);
        }
        public ActionResult viewhypertensionsymptoms()
        {
            return View();
        }

        #region Auto Complete Diabetes Content
        public ActionResult AutocompleteDiabetesContent(string searchstring)
        {
            JsonResult strSearch = _cdhsCommonMethods.ContentTypeGetSeacrchString(searchstring);
            return Json(strSearch, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Auto Complete Diabetes Category
        public ActionResult AutocompleteDiabetesCategory(string searchstring)
        {
            JsonResult strSearch = _cdhsCommonMethods.ContentCategoryGetSeacrchString(searchstring);
            return Json(strSearch, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Auto Complete Diabetes Company
        public ActionResult AutocompleteDiabetesCompany(string searchstring)
        {
            JsonResult strSearch = _cdhsCommonMethods.ContentCompanyGetSeacrchString(searchstring);
            return Json(strSearch, JsonRequestBehavior.AllowGet);
        }
        #endregion
	}
}