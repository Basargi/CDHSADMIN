﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class DiabetesReportViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "State")]
        public IEnumerable<StateListModel> StateList { get; set; }
        public string State { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "City")]
        public IEnumerable<CityListModel> CityList { get; set; }
        public string City { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Age")]
        public IEnumerable<AgeListModel> AgeList { get; set; }
        public string Age { get; set; }

        public bool Male { get; set; }

        public bool Female { get; set; }

        public string Search { get; set; }

        public string SelectedSearchByState { get; set; }

        public string SelectedSearchByCity { get; set; }

        public string SelectedSearchByAge { get; set; }


    }
}