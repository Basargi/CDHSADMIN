﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class ViewHypertensionSymptomsViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "Approval")]
        public IEnumerable<ApprovalListModel> ApprovalList { get; set; }
        public string Approval { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Asset")]
        public IEnumerable<AssetListModel> AssetList { get; set; }
        public string Asset { get; set; }

        public string SearchByContentType { get; set; }

        public string SearchByContentCategory { get; set; }

        public string SearchByContentSponsors { get; set; }

        public bool Loremipsumdolor { get; set; }

        public bool SelectedApprovalStatus { get; set; }

        public string SelectedAssetStatus { get; set; }





    }
}