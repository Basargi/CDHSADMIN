﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class ViewVideoChannelViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "Approval")]
        public IEnumerable<ApprovalListModel> ApprovalList { get; set; }
        public string Approval { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Action")]
        public IEnumerable<ActionListModel> ActionList { get; set; }
        public string Action { get; set; }

        public bool Enable { get; set; }

        public string SelectedApprovalStatus { get; set; }

        public string SelectedActionStatus { get; set; }



    }
}