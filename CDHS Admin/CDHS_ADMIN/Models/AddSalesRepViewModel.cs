﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class AddSalesRepViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailID { get; set; }

        public string EnterPassword { get; set; }

        public string ConfirmPassword { get; set; }

        public string MobileNo { get; set; }

        public string Subrub { get; set; }

        public string State { get; set; }

        public string Postcode { get; set; }

        public string id { get; set; }

        public string Priority { get; set; }

    }
}