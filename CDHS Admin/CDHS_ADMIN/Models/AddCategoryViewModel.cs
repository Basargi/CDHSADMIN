﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class AddCategoryViewModel
    {
        public string CategoryName { get; set; }

        public string CategoryDescription { get; set; }

        public string id { get; set; }

        public string Priority { get; set; }

    }
}