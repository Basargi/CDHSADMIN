﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class ViewContentTypeViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "Upcoming")]
        public IEnumerable<UpcomingListModel> UpcomingList { get; set; }
        public string Upcoming { get; set; }

        public string SelectedSearch { get; set; }

        public bool Event { get; set; }

        public bool News { get; set; }

        public bool Videos { get; set; }

        public bool Photos { get; set; }

        public string SelectedUpcomingEvents { get; set; }

        public IEnumerable<Content_Types> contentcdhs { get; set; }

    }
}