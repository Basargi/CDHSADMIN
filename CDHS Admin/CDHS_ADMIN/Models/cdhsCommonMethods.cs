﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Web.Hosting;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Collections;


namespace CDHS_ADMIN.Models
{
    public class cdhsCommonMethods
    {
        private CDHSEntities1 db = new CDHSEntities1();

        #region Content Type Get Seacrch String
        public JsonResult ContentTypeGetSeacrchString(string searchstring)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            JsonResult jr = null;
            var query = from q in db.Content_Types select new { value = q.id, label = q.Type_Name };
            var typelist = query;
            if (!string.IsNullOrEmpty(searchstring))
            {
                typelist = query.Where(q => q.label.ToLower().StartsWith(searchstring));
            }
            string json = serializer.Serialize(typelist);
            var result = new JsonResult
            {
                Data = typelist
            };
            return result;
        }
        #endregion

        #region Content Category Get Seacrch String
        public JsonResult ContentCategoryGetSeacrchString(string searchstring)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            JsonResult jr = null;
            var query = from q in db.Content_Category select new { value = q.id, label = q.Category_Name };
            var typelist = query;
            if (!string.IsNullOrEmpty(searchstring))
            {
                typelist = query.Where(q => q.label.ToLower().StartsWith(searchstring));
            }
            string json = serializer.Serialize(typelist);
            var result = new JsonResult
            {
                Data = typelist
            };
            return result;
        }
        #endregion

        #region Content Company Get Seacrch String
        public JsonResult ContentCompanyGetSeacrchString(string searchstring)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            JsonResult jr = null;
            var query = from q in db.Sponsors select new { value = q.id, label = q.Company_Name };
            var typelist = query;
            if (!string.IsNullOrEmpty(searchstring))
            {
                typelist = query.Where(q => q.label.ToLower().StartsWith(searchstring));
            }
            string json = serializer.Serialize(typelist);
            var result = new JsonResult
            {
                Data = typelist
            };
            return result;
        }
        #endregion

        #region Content Symptoms Get Seacrch String
        public JsonResult ContentSymptomsGetSeacrchString(string searchstring)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            JsonResult jr = null;
            var query = from q in db.Sponsors select new { value = q.id, label = q.id };
            var typelist = query;
            if (!string.IsNullOrEmpty(searchstring))
            {
                typelist = query.Where(q => q.label.ToLower().StartsWith(searchstring));
            }
            string json = serializer.Serialize(typelist);
            var result = new JsonResult
            {
                Data = typelist
            };
            return result;
        }
        #endregion

        #region AboutCDHS Methods
        public int addaboutcdhsAdd(About_cdhs _Aboutcdhs)
        {
            int result = 0;
            try
            {
                db.About_cdhs.Add(_Aboutcdhs);
                db.SaveChanges();
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }

        public IEnumerable<About_cdhs> GetAboutcdhsData()
        {
            List<About_cdhs> _About_cdhs = new List<About_cdhs>();
            var query = from q in db.About_cdhs select q;
            _About_cdhs = query.ToList();
            return _About_cdhs;
        }
        public About_cdhs GetDataById(string id)
        {
            About_cdhs _About_cdhs = new About_cdhs();
            _About_cdhs = db.About_cdhs.Find(id);
            return _About_cdhs;
        }
        #endregion 

        #region CategoryCDHS Methods
        public int addcategoryAdd(Content_Category _Contcategory)
        {
            int result = 0;
            try
            {
                db.Content_Category.Add(_Contcategory);
                db.SaveChanges();
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }

        public IEnumerable<Content_Category> GetCategorycdhsData()
        {
            List<Content_Category> _Content_Category = new List<Content_Category>();
            var query = from q in db.Content_Category select q;
            _Content_Category = query.ToList();
            return _Content_Category;
        }
        public Content_Category GetDatacategoryById(string id)
        {
            Content_Category _Content_Category = new Content_Category();
            _Content_Category = db.Content_Category.Find(id);
            return _Content_Category;
        }
        #endregion

        #region ContentCDHS Methods
        public int addcontenttypeAdd(Content_Types _Conttype)
        {
            int result = 0;
            try
            {
                db.Content_Types.Add(_Conttype);
                db.SaveChanges();
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }
        public IEnumerable<Content_Types> GetContentcdhsData()
        {
            List<Content_Types> _Content_Types = new List<Content_Types>();
            var query = from q in db.Content_Types select q;
            _Content_Types = query.ToList();
            return _Content_Types;
        }
        public Content_Types GetDatacontentById(string id)
        {
            Content_Types _Content_Types = new Content_Types();
            _Content_Types = db.Content_Types.Find(id);
            return _Content_Types;
        }
        #endregion

        #region sponsorsCDHS Methods
        public int addsponsorsAdd(Sponsor _sponsor)
        {
            int result = 0;
            try
            {
                db.Sponsors.Add(_sponsor);
                db.SaveChanges();
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }
        public IEnumerable<Sponsor> GetSponsorscdhsData()
        {
            List<Sponsor> _Sponsors = new List<Sponsor>();
            var query = from q in db.Sponsors select q;
            _Sponsors = query.ToList();
            return _Sponsors;
        }
        public Sponsor GetDataSponsorsById(string id)
        {
            Sponsor _Sponsors = new Sponsor();
            _Sponsors = db.Sponsors.Find(id);
            return _Sponsors;
        }
        #endregion

        #region addaboutcdhsEdit
        public int addaboutcdhsEdit(About_cdhs _Aboutcdhs)
        {
            int result = 0;
            try
            {
                db.Entry(_Aboutcdhs).State = EntityState.Modified;
                db.SaveChanges();
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }
        #endregion

        #region addcategoryEdit
        public int addcategoryEdit(Content_Category Cont_Category)
        {
            int result = 0;
            try
            {
                db.Entry(Cont_Category).State = EntityState.Modified;
                db.SaveChanges();
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }
        #endregion

        #region addcontenttypeEdit
        public int addcontenttypeEdit(Content_Types Cont_types)
        {
            int result = 0;
            try
            {
                db.Entry(Cont_types).State = EntityState.Modified;
                db.SaveChanges();
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }
        #endregion

        #region addsponsorsEdit
        public int addsponsorsEdit(Sponsor _Sponsors)
        {
            int result = 0;
            try
            {
                db.Entry(_Sponsors).State = EntityState.Modified;
                db.SaveChanges();
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }
        #endregion

        #region GPCDHS Methods
        public int addgpAdd(GP _GP)
        {
            int result = 0;
            try
            {
                db.GPs.Add(_GP);
                db.SaveChanges();
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }
        public IEnumerable<GP> GetGpcdhsData()
        {
            List<GP> _GP = new List<GP>();
            var query = from q in db.GPs select q;
            _GP = query.ToList();
            return _GP;
        }
        public GP GetDataGpById(string id)
        {
            GP _GP = new GP();
            _GP = db.GPs.Find(id);
            return _GP;
        }
        #endregion

        #region addgpEdit
        public int addgpEdit(GP _GP)
        {
            int result = 0;
            try
            {
                db.Entry(_GP).State = EntityState.Modified;
                db.SaveChanges();
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }
        #endregion

        #region DiabetesContentCDHS Methods
        public int adddiabetescontentAdd(Diabetes_Content _Diabetes_Content)
        {
            int result = 0;
            try
            {
                db.Diabetes_Content.Add(_Diabetes_Content);
                db.SaveChanges();
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }

        public IEnumerable<Diabetes_Content> GetDiabetesContentcdhsData()
        {
            List<Diabetes_Content> _Diabetes_Content = new List<Diabetes_Content>();
            var query = from q in db.Diabetes_Content select q;
            _Diabetes_Content = query.ToList();
            return _Diabetes_Content;
        }
        public Diabetes_Content GetDataDiabetesContentById(string id)
        {
            Diabetes_Content _Diabetes_Content = new Diabetes_Content();
            _Diabetes_Content = db.Diabetes_Content.Find(id);
            return _Diabetes_Content;
        }
        #region addgpEdit
        public int adddiabetescontentEdit(Diabetes_Content _Diabetes_Content)
        {
            int result = 0;
            try
            {
                db.Entry(_Diabetes_Content).State = EntityState.Modified;
                db.SaveChanges();
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }
        #endregion
        #endregion 


        #region SalesCDHS Methods
        public int addsalesrepcdhsAdd(Sales_Rep _Sales_Rep)
        {
            int result = 0;
            try
            {
                db.Sales_Rep.Add(_Sales_Rep);
                db.SaveChanges();
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }
        public IEnumerable<Sales_Rep> GetSalesrepcdhsData()
        {
            List<Sales_Rep> _Sales_Rep = new List<Sales_Rep>();
            var query = from q in db.Sales_Rep select q;
            _Sales_Rep = query.ToList();
            return _Sales_Rep;
        }
        public Sales_Rep GetDataSalesrepById(string id)
        {
            Sales_Rep _Sales_Rep = new Sales_Rep();
            _Sales_Rep = db.Sales_Rep.Find(id);
            return _Sales_Rep;
        }
        #endregion 
        #region
        public int addsalesrepcdhsEdit(Sales_Rep _Sales_Rep)
        {
            int result = 0;
            try
            {
                db.Entry(_Sales_Rep).State = EntityState.Modified;
                db.SaveChanges();
                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }
        #endregion

    }
}
