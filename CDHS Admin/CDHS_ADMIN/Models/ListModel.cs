﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CDHS_ADMIN.Models
{
    public class ListModel
    {

    }
    public class PriorityListModel
    {
        public string id { get; set; }
        public string Priority { get; set; }
    }
    public class ApprovalListModel
    {
        public string ApprovalId { get; set; }
        public string ApprovalStatus { get; set; }
    }
    public class AssetListModel
    {
        public string AssetId { get; set; }
        public string AssetStatus { get; set; }
    }
    public class StateListModel
    {
        public string StateId { get; set; }
        public string StateStatus { get; set; }
    }
    public class CityListModel
    {
        public string CityId { get; set; }
        public string CityStatus { get; set; }
    }
    public class AgeListModel
    {
        public string AgeId { get; set; }
        public string AgeStatus { get; set; }
    }
    public class SearchListModel
    {
        public string SearchId { get; set; }
        public string SearchStatus { get; set; }
    }
    public class LinkListModel
    {
        public string LinkId { get; set; }
        public string LinkStatus { get; set; }
    }
    public class ActionListModel
    {
        public string ActionId { get; set; }
        public string ActionStatus { get; set; }
    }
    public class UpcomingListModel
    {
        public string UpcomingId { get; set; }
        public string UpcomingStatus { get; set; }
    }
}