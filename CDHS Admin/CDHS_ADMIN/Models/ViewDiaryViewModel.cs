﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class ViewDiaryViewModel
    {
        public string Description { get; set; }

        public bool BeforeBreakfast { get; set; }

        public bool BeforeLunch { get; set; }

        public bool BeforeDinner { get; set; }

        public bool AfterDinner { get; set; }




    }
}