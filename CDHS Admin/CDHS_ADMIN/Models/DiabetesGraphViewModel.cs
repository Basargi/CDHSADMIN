﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CDHS_ADMIN.Models
{
    public class DiabetesGraphViewModel
    {
        public bool DataCalculated { get; set; }

        public bool BeforeBreakfast { get; set; }

        public bool AfterBreakfast { get; set; }

        public bool BeforeLunch { get; set; }

        public bool AfterLunch { get; set; }

        public bool BeforeDinner { get; set; }

        public bool AfterDinner { get; set; }

        
    }
}