﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class UserRightsViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "Link")]
        public IEnumerable<LinkListModel> LinkList { get; set; }
        public string Link { get; set; }

        public string SearchByName { get; set; }

        public string SelectedLink { get; set; }


    }
}