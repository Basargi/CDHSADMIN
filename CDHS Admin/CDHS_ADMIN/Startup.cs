﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CDHS_ADMIN.Startup))]
namespace CDHS_ADMIN
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
